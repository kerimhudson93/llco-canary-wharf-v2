import { atom, useAtom } from "jotai";
import { useEffect, useCallback } from "react";
import throttle from "lodash.throttle";
const scrollDistanceAtom = atom(0);

/**
 * This adds a listener to the scroll length, so we can get an idea of what page we are currently on.
 * @returns
 */
export const useScroll = () => {
  const [scroll, setScroll] = useAtom(scrollDistanceAtom);
  // eslint-disable-next-line

  const scrollListener = useCallback(
    throttle(() => {
      setScroll(
        window.scrollY /
          (window.innerHeight *
            (document.body.scrollHeight / window.innerHeight - 1))
      );
    }, 50),
    []
  );

  useEffect(() => {
    window.addEventListener("scroll", scrollListener);

    return () => {
      window.removeEventListener("scroll", scrollListener);
    };
  }, [scrollListener]);

  return { scroll };
};

export const usePage = () => {
  const [scroll] = useAtom(scrollDistanceAtom);
  const range = (start, distance) => {
    const end = start + distance;
    return scroll < start
      ? 0
      : scroll > end
      ? 1
      : (scroll - start) / (end - start);
  };

  const curve = (from, distance, margin = 0) => {
    return Math.sin(range(from, distance, margin) * Math.PI);
  };

  const pages = {
    0: "video",
    1: "slide",
    2: "slide",
    3: "overview",
    4: "overview",
    5: "floor47",
    6: "floor47",
    7: "floor48",
    8: "floor48",
    9: "floor49",
    10: "floor49",
    11: "floor45",
    12: "floor45",
    13: "floor40",
    14: "floor40",
    15: "gallery",
    16: "contact",
    17: "contact",
  };

  const totalPages = 17;
  const currPageNumber = Math.floor(scroll * totalPages);
  const currentPage = { number: currPageNumber, id: pages[currPageNumber] };
  return { page: currentPage, pages, totalPages, range, curve };
};

const innerWidthAtom = atom(false);

export const useMobileListener = () => {
  const [, setInnerWidth] = useAtom(innerWidthAtom);

  useEffect(() => {
    setInnerWidth(window.innerWidth);
  }, [setInnerWidth]);

  const handleWindowSizeChange = useCallback(() => {
    setInnerWidth(window.innerWidth);
  }, [setInnerWidth]);

  useEffect(() => {
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, [handleWindowSizeChange]);
};

export const useMobile = () => {
  const [innerWidth] = useAtom(innerWidthAtom);

  return innerWidth <= 768;
};

const pointsAtom = atom();
const activePointAtom = atom();
const showActivePointOverlayAtom = atom(false);
export const usePoints = () => {
  const [points, setPoints] = useAtom(pointsAtom);
  const [activePoint, setActivePoint] = useAtom(activePointAtom);
  const [showActivePointOverlay, setShowActivePointOverlay] = useAtom(
    showActivePointOverlayAtom
  );
  return {
    points,
    activePoint,
    setPoints,
    setActivePoint,
    showActivePointOverlay,
    setShowActivePointOverlay,
  };
};
