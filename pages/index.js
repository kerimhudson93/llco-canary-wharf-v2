import { Preload, Plane, Html, Text, useEdgeSplit } from "@react-three/drei";
import { usePage, useScroll, useMobileListener, usePoints } from "../atoms";
import { Canvas } from "@react-three/fiber";
import { Suspense, useEffect } from "react";
import HeroVideo from "../components/HeroVideo";
import SideNavigation from "../components/SideNavigation";
import Tower from "../components/Tower";
import * as THREE from "three";
import LoadingScreen from "../components/LoadingScreen";
import Header from "../components/Header";
import CanvasHeadline from "../components/CanvasHeadline";
import Points from "../components/Points";
import Contact from "../components/Contact";
import contentful from "../contentful";
import PointOverlay from "../components/PointOverlay";
import SlideOver from "../components/SlideOver";

const Home = ({ points, floors, availability, gallery }) => {
  const { setPoints } = usePoints();

  useEffect(() => {
    setPoints(points);
  }, [points, setPoints]);

  useScroll();
  useMobileListener();
  return (
    <>
      <div
        className="bg-cream fixed top-0 left-0 w-full h-screen"
        style={{ zIndex: "-2" }}
      />
      <LoadingScreen />
      <Header />
      <SideNavigation />
      <PointOverlay />
      <SlideOver />
      <HeroVideo />

      <div id="slidein" className="w-screen h-2screen" />
      <div id="overview" className="w-screen h-2screen" />
      <div id="floor47" className="h-2screen w-screen" />
      <div id="floor48" className="h-2screen w-screen" />
      <div id="floor49" className="h-2screen w-screen" />
      <div id="floor45" className="h-2screen w-screen" />
      <div id="floor40" className="h-screen w-screen" />
      <div
        id="gradient"
        className="h-screen w-screen from-transparent to-cream  bg-gradient-to-b z-50 relative"
      />
      <div id="gallery" className="relative h-screen w-screen bg-cream z-50" />
      <Contact />

      <Canvas
        id="canvas-wrapper"
        camera={{ position: [0, 2, 4], fov: 50 }}
        gl2={false}
        dpr={2}
        gl={{
          antialias: true,
          outputEncoding: THREE.sRGBEncoding,
          powerPreference: "high-performance",
        }}
      >
        <Suspense fallback={null}>
          <Tower />
          <Points />
          <CanvasHeadline />
          <Preload />
        </Suspense>
      </Canvas>
    </>
  );
};

export const getStaticProps = async () => {
  let floors = await contentful.getEntries({ content_type: "floor" });
  let gallery = await contentful.getEntries({ content_type: "gallery" });

  floors = floors.items.map((floor) => floor.fields);

  let points = {};
  let availability = {};

  floors.map((floor) => {
    let currentFloorPoints = floor.points || [];
    let currentFloorAvailability = floor.availability || [];

    currentFloorAvailability = currentFloorAvailability.map((available) => ({
      ...available.fields,
      id: available.sys.id,
    }));

    currentFloorPoints = currentFloorPoints.map((point) => ({
      ...point.fields,
      id: point.sys.id,
    }));

    points[floor.referenceId] = currentFloorPoints;
    availability[floor.referenceId] = currentFloorAvailability;
  });

  gallery = gallery?.items.map((image) => ({
    image: image.fields.image,
    caption: image.fields.caption,
    id: image.sys.id,
  }));

  return {
    props: {
      points,
      floors: floors.sort((a, b) => a.order - b.order),
      availability,
      gallery,
    },
  };
};

export default Home;
