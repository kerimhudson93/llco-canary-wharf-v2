import { Transition } from "@headlessui/react";
import { Fragment, useEffect, useState } from "react";

const LoadingScreen = () => {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    document.body.style.overflow = "hidden";

    setTimeout(() => {
      setLoaded(true);
      setTimeout(() => (document.body.style.overflow = "visible"), 1000);
    }, 500);
  }, []);

  return (
    <Transition
      as={Fragment}
      show={!loaded}
      leave="transform transition-all ease-in-out duration-1000"
      leaveFrom="opacity-100"
      leaveTo="opacity-0"
    >
      <div className="fixed top-0 left-0 w-screen h-screen bg-black flex items-center justify-center z-50">
        <h2 className="text-white uppercase font-sans font-light text-6xl">
          One Canada Square
        </h2>
      </div>
    </Transition>
  );
};

export default LoadingScreen;
