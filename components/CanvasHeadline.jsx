import { Text, Plane } from "@react-three/drei";
import { useFrame, useThree } from "@react-three/fiber";
import { useRef } from "react";
import { usePage } from "../atoms";
import { lerp } from "../utils";
const CanvasHeadline = () => {
  const group = useRef();

  const { range, curve, totalPages } = usePage();
  const { viewport } = useThree();

  const planeRange = range(1 / totalPages, 2 / totalPages);
  const textRange = range(0.5 / totalPages, 0.5 / totalPages);

  useFrame(() => {
    if (group.current) {
      group.current.position.y = lerp(
        group.current.position.y,
        Math.min(textRange * 6 - 5, 1.5)
      );
    }
  });

  return planeRange >= 1 ? null : (
    <>
      <group ref={group} rotation={[-0.4, 0, 0]} position={[0, -5, -0.5]}>
        <Text
          color="#FDF7F1"
          fillOpacity={planeRange === 1 ? 0 : 1}
          position={[0, 0, 0]}
          fontSize={0.07 * viewport.width}
          font="/fonts/FoundersGrotesk-Regular.otf"
        >
          THE HIGHEST LEVEL
        </Text>
        <Text
          color="#FDF7F1"
          transparent
          fillOpacity={planeRange === 1 ? 0 : 1}
          position={[0, -0.07 * viewport.width, 0]}
          fontSize={0.07 * viewport.width}
          font="/fonts/FoundersGrotesk-Regular.otf"
        >
          FOR GLOBAL BUSINESS
        </Text>
      </group>
      <Plane args={[100, 100]} position={[0, 0, -5]}>
        <meshBasicMaterial
          color="#303B3C"
          transparent
          opacity={1 - planeRange}
        />
      </Plane>
    </>
  );
};

export default CanvasHeadline;
