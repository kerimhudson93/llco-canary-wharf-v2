import Link from "next/link";
import { Dialog, Transition } from "@headlessui/react";
import { Fragment, useEffect, useState } from "react";
import { usePage } from "../atoms";
const Header = () => {
  const [open, setOpen] = useState(false);

  const { page } = usePage();

  const isDarkBackground =
    page.number === 0 || page.number === 1 || page.number > 16;

  return (
    <>
      <header
        className={[
          "fixed top-0 left-0 w-full flex justify-between items-center py-4 px-8 transition-opacity duration-200 z-100",
          page.number === 0 ? "opacity-0" : "opacity-100",
        ].join(" ")}
      >
        <Link href="/">
          <a className="hidden  lg:block">
            <h1
              className={[
                "uppercase font-light text-3xl transition-colors duration-200",
                isDarkBackground ? "text-white" : "text-darkgreen",
              ].join(" ")}
            >
              One Canada Square
            </h1>
          </a>
        </Link>
        <div className="flex flex-1 justify-end items-center">
          <button
            className={[
              "w-8 h-8 transition-colors duration-200",
              isDarkBackground ? "text-white" : "text-darkgreen",
            ].join(" ")}
            onClick={() => setOpen(true)}
          >
            <svg
              width="auto"
              height="auto"
              viewBox="0 0 32 38"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M32 28H0M32 10H0M32 37H0M32 19H0M32 1H0"
                stroke="currentColor"
                strokeWidth="2"
              />
            </svg>
          </button>
        </div>
      </header>
      <div />
      <Transition.Root show={open} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 overflow-hidden z-50"
          onClose={setOpen}
        >
          <div className="absolute inset-0 overflow-hidden">
            <Transition.Child
              as={Fragment}
              enter="transition-opacity ease-in-out duration-500 sm:duration-700"
              enterFrom="opacity-0"
              enterTo="opacity-50"
              leave="transition-opacity ease-in-out duration-500 sm:duration-700"
              leaveFrom="opacity-50"
              leaveTo="opacity-0"
            >
              <div>
                <Dialog.Overlay className="absolute inset-0 bg-cream opacity-50" />
              </div>
            </Transition.Child>

            <div className="fixed inset-y-0 right-0 pl-10 max-w-full flex">
              <Transition.Child
                as={Fragment}
                enter="transform transition ease-in-out duration-500 sm:duration-700"
                enterFrom="translate-x-full"
                enterTo="translate-x-0"
                leave="transform transition ease-in-out duration-500 sm:duration-700"
                leaveFrom="translate-x-0"
                leaveTo="translate-x-full"
              >
                <div className="w-screen max-w-3xl">
                  <div className="bg-darkgreen w-full h-full text-white flex flex-col px-16 py-8">
                    <div className="w-full flex justify-end">
                      <button
                        onClick={() => setOpen(false)}
                        className="w-8 h-8"
                      >
                        <svg
                          width="auto"
                          height="auto"
                          viewBox="0 0 25 25"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M23.6274 1.00002L1 23.6274M23.6274 1.00002L1 23.6274"
                            stroke="currentColor"
                            strokeWidth="2"
                          />
                          <path
                            d="M0.999537 1.001L23.627 23.6284M0.999537 1.001L23.627 23.6284"
                            stroke="currentColor"
                            strokeWidth="2"
                          />
                        </svg>
                      </button>
                    </div>
                    <nav className="flex-1 flex flex-col text-5xl gap-8 pt-16 font-light">
                      <a onClick={() => setOpen(false)} href="#video">
                        Introduction
                      </a>
                      <a onClick={() => setOpen(false)} href="#overview">
                        Building Overview
                      </a>
                      <a onClick={() => setOpen(false)} href="#gallery">
                        Gallery
                      </a>
                      <a onClick={() => setOpen(false)} href="#contact">
                        Enquire
                      </a>
                    </nav>
                    <nav className="flex flex-col gap-2 opacity-50">
                      <span>
                        &copy; One Canada Square {new Date().getFullYear()}
                      </span>
                      <a>Cookie Policy</a>
                      <a>Privacy Policy</a>
                      <a>Terms &amp; Conditions</a>
                      <a>Cookie Policy</a>
                    </nav>
                  </div>
                </div>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition.Root>
    </>
  );
};

export default Header;
