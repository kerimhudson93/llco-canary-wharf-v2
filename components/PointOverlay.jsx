import { useEffect, Fragment, useState } from "react";
import { usePoints } from "../atoms";
import { Transition } from "@headlessui/react";
const PointOverlay = () => {
  const { points, activePoint, setActivePoint, setShowActivePointOverlay } =
    usePoints();

  const [allPoints, setAllPoints] = useState([]);

  useEffect(() => {
    const p = [];
    if (points) {
      Object.values(points).map((a) => p.push(...a));
    }
    setAllPoints(p);
  }, [points]);

  const currentPoint = allPoints.filter((a) => a.id === activePoint)[0];
  console.log(currentPoint);

  return (
    <Transition
      show={activePoint ? true : false}
      as={Fragment}
      enter="transition-opacity ease-in-out duration-300"
      enterFrom="opacity-0"
      enterTo="opacity-100"
      leave="opacity-0"
      leaveFrom="opacity-0"
      leaveTo="opacity-0"
    >
      <div
        className={[
          "fixed bottom-4 px-4 transform w-full lg:max-w-md max-h-64 z-50",
          currentPoint?.horizontalAlignment === "left"
            ? "lg:left-1/4 lg:right-auto lg:-translate-x-1/2 "
            : "lg:right-1/4 lg:left-auto lg:translate-x-1/2 ",
          currentPoint?.verticalAlignment === "bottom"
            ? "lg:bottom-1/3"
            : currentPoint?.verticalAlignment === "top"
            ? "lg:top-1/3"
            : "lg:top-1/2 lg:-translate-y-1/2",
        ].join(" ")}
      >
        <div
          className="bg-darkgreen px-6 py-4 text-cream lg:text-darkgreen lg:bg-cream border shadow-lg"
          style={{ borderColor: "rgba(48, 59, 60, 0.5)" }}
        >
          <div className="flex justify-between items-center border-b border-cream lg:border-darkgreen pb-2">
            <h4 className="uppercase text-xl">{currentPoint?.title}</h4>
            <button className="w-4 h-4" onClick={() => setActivePoint(null)}>
              <svg
                width="auto"
                height="auto"
                viewBox="0 0 25 25"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M23.6274 1.00002L1 23.6274M23.6274 1.00002L1 23.6274"
                  stroke="currentColor"
                  strokeWidth="2"
                />
                <path
                  d="M0.999537 1.001L23.627 23.6284M0.999537 1.001L23.627 23.6284"
                  stroke="currentColor"
                  strokeWidth="2"
                />
              </svg>
            </button>
          </div>
          <div className="py-2">
            <p className="font-light">{currentPoint?.description}</p>
            <button
              onClick={() => setShowActivePointOverlay(true)}
              className="uppercase mt-4"
            >
              More details
            </button>
          </div>
        </div>
      </div>
    </Transition>
  );
};

export default PointOverlay;
