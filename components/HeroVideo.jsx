import { Fragment, useEffect, useState } from "react";
import { Transition } from "@headlessui/react";
const HeroVideo = () => {
  const [showTitle, setShowTitle] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setShowTitle(false);
    }, 6000);
  }, []);

  return (
    <div
      id="video"
      className="w-screen h-screen bg-black relative flex items-center justify-center z-10"
    >
      <Transition
        as={Fragment}
        show={showTitle}
        enter="duration-200 transition-all ease-in-out"
        enterFrom="opacity-100"
        enterTo="opacity-0"
        leave="transition-all ease-in-out duration-1000"
        leaveFrom="opacity-100"
        leaveTo="opacity-0"
      >
        <h2 className="text-white text-6xl font-light uppercase z-10 text-center">
          One Canada Square
        </h2>
      </Transition>
      <Transition
        as={Fragment}
        show={!showTitle}
        enter="duration-500 transition-all ease-in-out delay-3000"
        enterFrom="opacity-0"
        enterTo="opacity-100"
        leave="transition-all ease-in-out duration-1000"
        leaveFrom="opacity-100"
        leaveTo="opacity-0"
      >
        <div className="z-10 text-white absolute bottom-12 left-1/2 -translate-x-1/2 transform">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="20"
            height="66"
            fill="none"
            viewBox="0 0 20 66"
          >
            <path
              stroke="currentColor"
              strokeWidth="2"
              d="M10 0v64m0 0l-9-9m9 9l9-9"
            ></path>
          </svg>
        </div>
      </Transition>
      <video
        autoPlay
        muted
        loop
        className="w-screen h-screen object-cover bg-black absolute top-0 left-0"
      >
        <source src="/video.mp4" type="video/mp4" />
      </video>
    </div>
  );
};

export default HeroVideo;
