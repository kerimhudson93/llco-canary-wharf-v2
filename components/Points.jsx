import { Html } from "@react-three/drei";
import { Transition } from "@headlessui/react";
import { Fragment, useEffect, useState } from "react";
import { usePage, usePoints } from "../atoms";
const Points = () => {
  const { points, setActivePoint, activePoint } = usePoints();
  const { page } = usePage();

  useEffect(() => {
    setActivePoint(null);
  }, [page.id, setActivePoint]);

  return (
    <>
      {points[page.id]?.map((point) => (
        <Html key={point.id} position={JSON.parse(point.coordinates)}>
          <Transition
            show={true}
            as={Fragment}
            enter="transition-opacity ease-in-out duration-500 sm:duration-700"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="transition-opacity ease-in-out duration-500 sm:duration-700"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="opacity-0 relative">
              {activePoint === point.id ? (
                <div className="w-4 h-4 bg-darkgreen rounded-full absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2" />
              ) : null}
              <div
                className={[
                  "w-6 h-6 bg-darkgreen rounded-full border-4  cursor-pointer",
                  activePoint === point.id ? " animate-ping" : "",
                ].join(" ")}
                onPointerDown={() => setActivePoint(point.id)}
              ></div>
            </div>
          </Transition>
        </Html>
      ))}
    </>
  );
};

export default Points;
