import React, { forwardRef } from "react";
import { useGLTF } from "@react-three/drei";
import useBakedTexture from "./useBakedTexture";
import Outline from "./Outline";

const Roof = forwardRef(({ ...props }, ref) => {
  const { nodes } = useGLTF("/models/roof_model.glb");
  const material = useBakedTexture("/models/roof_bake.jpg");
  return (
    <group ref={ref} {...props} dispose={null}>
      <Outline geometry={nodes.Roof.geometry} rotation={[Math.PI / 2, 0, 0]} />
      <mesh
        geometry={nodes.Roof.geometry}
        material={material}
        rotation={[Math.PI / 2, 0, 0]}
      />
    </group>
  );
});

Roof.displayName = "Roof";

useGLTF.preload("/models/roof_model.glb");
export default Roof;
