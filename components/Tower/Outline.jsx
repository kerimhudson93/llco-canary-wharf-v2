import { useMobile } from "../../atoms";

const Outline = ({ geometry, ...props }) => {
  const mobile = useMobile();
  return (
    <lineSegments {...props}>
      <edgesGeometry args={[geometry, 30]} />
      <lineBasicMaterial
        color={!mobile ? "#536365" : "#A9A9A9"}
        linewidth="1"
      />
    </lineSegments>
  );
};

export default Outline;
