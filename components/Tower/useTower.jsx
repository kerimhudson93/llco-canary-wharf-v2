import { useFrame, useThree } from "@react-three/fiber";
import useRefs from "react-use-refs";
import * as THREE from "three";
import { Camera } from "three";
import { useMobile, usePage, usePoints } from "../../atoms";
import { lerp } from "../../utils";

const DEFAULT_SPACING = 0.045;
const defaultPositions = {
  floor40: [0, -DEFAULT_SPACING * 3, 0],
  floor44: [0, -DEFAULT_SPACING, 0],
  floor45: [0, 0, 0],
  floor47: [0, DEFAULT_SPACING * 2, 0],
  floor48: [0, DEFAULT_SPACING * 3, 0],
  floor49: [0, DEFAULT_SPACING * 4, 0],
  tower: [0, 0, 0],
  restOfTower: [0, -DEFAULT_SPACING * 4, 0],
  roof: [0, DEFAULT_SPACING * 5, 0],
};

const MOVING_SPACING = 0.6;

const moveUp = (ref, floorName) => {
  ref.current.position.y = lerp(
    ref.current.position.y,
    defaultPositions[floorName][1] + MOVING_SPACING
  );
};

const moveDown = (ref, floorName) => {
  ref.current.position.y = lerp(
    ref.current.position.y,
    defaultPositions[floorName][1] - MOVING_SPACING
  );
};

const reset = (ref, floorName) => {
  ref.current.position.y = lerp(
    ref.current.position.y,
    defaultPositions[floorName][1]
  );
};

const useTower = () => {
  const { camera } = useThree();
  const { activePoint } = usePoints();
  const { page, range, totalPages } = usePage();
  const towerHeight = range(0, 1.5 / totalPages);
  const [
    floor40,
    floor44,
    floor45,
    floor47,
    floor48,
    floor49,
    tower,
    restOfTower,
    roof,
  ] = useRefs();

  const refs = {
    floor40,
    floor44,
    floor45,
    floor47,
    floor48,
    floor49,
    tower,
    restOfTower,
    roof,
  };

  const resetAllFloors = () => {
    reset(floor48, "floor48");
    reset(floor49, "floor49");
    reset(roof, "roof");
    reset(floor47, "floor47");
    reset(floor45, "floor45");
    reset(floor44, "floor44");
    reset(floor40, "floor40");
    reset(restOfTower, "restOfTower");
    tower.current.position.z = lerp(tower.current.position.z, 0);
    tower.current.position.y = lerp(tower.current.position.y, 0);
  };

  const showFloor47 = () => {
    moveUp(floor48, "floor48");
    moveUp(floor49, "floor49");
    moveUp(roof, "roof");
    reset(floor47, "floor47");
    moveDown(floor45, "floor45");
    moveDown(floor44, "floor44");
    moveDown(floor40, "floor40");
    moveDown(restOfTower, "restOfTower");
    tower.current.position.z = lerp(tower.current.position.z, 1);
    tower.current.position.y = lerp(tower.current.position.y, 1);
    tower.current.rotation.y = lerp(tower.current.rotation.y, 0.8);
  };

  const showFloor48 = () => {
    reset(floor48, "floor48");
    moveUp(floor49, "floor49");
    moveUp(roof, "roof");
    moveDown(floor47, "floor47");
    moveDown(floor45, "floor45");
    moveDown(floor44, "floor44");
    moveDown(floor40, "floor40");
    moveDown(restOfTower, "restOfTower");
    tower.current.position.z = lerp(tower.current.position.z, 1);
    tower.current.position.y = lerp(tower.current.position.y, 1);
  };

  const showFloor49 = () => {
    moveDown(floor48, "floor48");
    reset(floor49, "floor49");
    moveUp(roof, "roof");
    moveDown(floor47, "floor47");
    moveDown(floor45, "floor45");
    moveDown(floor44, "floor44");
    moveDown(floor40, "floor40");
    moveDown(restOfTower, "restOfTower");
    tower.current.position.z = lerp(tower.current.position.z, 1);
    tower.current.position.y = lerp(tower.current.position.y, 0);
  };

  const showFloor45 = () => {
    moveUp(floor48, "floor48");
    moveUp(floor49, "floor49");
    moveUp(roof, "roof");
    moveUp(floor47, "floor47");
    reset(floor45, "floor45");
    moveDown(floor44, "floor44");
    moveDown(floor40, "floor40");
    moveDown(restOfTower, "restOfTower");
    tower.current.position.z = lerp(tower.current.position.z, 1);
    tower.current.position.y = lerp(tower.current.position.y, 0.5);
  };

  const showFloor40 = () => {
    moveUp(floor48, "floor48");
    moveUp(floor49, "floor49");
    moveUp(roof, "roof");
    moveUp(floor47, "floor47");
    moveUp(floor45, "floor45");
    moveUp(floor44, "floor44");
    reset(floor40, "floor40");
    moveDown(restOfTower, "restOfTower");
    tower.current.position.z = lerp(tower.current.position.z, 1);
    tower.current.position.y = lerp(tower.current.position.y, 1);
    tower.current.rotation.y = lerp(tower.current.rotation.y, -1);
  };

  useFrame((state, delta) => {
    if (
      floor40.current &&
      floor44.current &&
      floor45.current &&
      floor47.current &&
      floor48.current &&
      floor49.current &&
      roof.current &&
      restOfTower.current &&
      tower.current
    ) {
      tower.current.position.y = lerp(
        tower.current.position.y,
        -4 * (1 - towerHeight)
      );

      tower.current.rotation.y = lerp(
        tower.current.rotation.y,
        -4 * (1 - towerHeight) + 0.6
      );

      switch (page.number) {
        case 5:
        case 6:
          return showFloor47();
        case 7:
        case 8:
          return showFloor48();
        case 9:
        case 10:
          return showFloor49();
        case 11:
        case 12:
          return showFloor45();
        case 13:
        case 14:
          return showFloor40();
        default:
          return resetAllFloors();
      }
    }
  });

  return {
    defaultPositions,
    refs,
    spacing: DEFAULT_SPACING,
  };
};

export default useTower;
