import { useThree, useLoader } from "@react-three/fiber";
import * as THREE from "three";

const useBakedTexture = (filePath) => {
  const three = useThree();
  const bakedTexture = useLoader(THREE.TextureLoader, filePath);

  bakedTexture.flipY = false;
  bakedTexture.encoding = THREE.sRGBEncoding;
  bakedTexture.minFilter = THREE.LinearFilter;
  bakedTexture.generateMipmaps = false;
  const bakedMaterial = new THREE.MeshBasicMaterial({
    map: bakedTexture,
  });

  bakedTexture.anisotropy = three.gl.capabilities.getMaxAnisotropy();

  return bakedMaterial;
};

export default useBakedTexture;
