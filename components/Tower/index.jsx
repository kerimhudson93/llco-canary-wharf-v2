import LowerFloor from "./LowerFloor";
import UpperFloor from "./UpperFloor";
import Roof from "./Roof";
import useTower from "./useTower";
import { useEffect, useState } from "react";
import { useMobile, usePage } from "../../atoms";

const Tower = () => {
  const { refs, spacing, defaultPositions } = useTower();
  const [restOfTower, setRestOfTower] = useState([]);
  const { page } = usePage();
  useEffect(() => {
    const towers = [];
    for (let i = 0; i < 50; i++) {
      towers.push(
        <LowerFloor key={`lowerfloor-${i}`} position={[0, -spacing * i, 0]} />
      );
    }
    setRestOfTower(towers);
  }, [spacing]);

  const isMobile = useMobile();
  return (
    <>
      <group
        ref={refs.tower}
        rotation={[0, 0.6, 0]}
        position={[0, 0, 0]}
        scale={isMobile ? 1.25 : 2}
      >
        <group position={defaultPositions.roof} ref={refs.roof}>
          <Roof position={[0, spacing, 0]} />
          <UpperFloor position={[0, 0, 0]} />
        </group>
        <UpperFloor ref={refs.floor49} position={defaultPositions.floor49} />
        <UpperFloor ref={refs.floor48} position={defaultPositions.floor48} />
        <UpperFloor
          ref={refs.floor47}
          position={defaultPositions.floor47}
          showCovers={page.number !== 5 && page.number !== 6}
          showBalcony
        />
        <group position={defaultPositions.floor45} ref={refs.floor45}>
          <LowerFloor position={[0, spacing, 0]} />
          <LowerFloor position={[0, 0, 0]} />
        </group>
        <LowerFloor ref={refs.floor44} position={defaultPositions.floor44} />
        <group ref={refs.floor40} position={defaultPositions.floor40}>
          <LowerFloor position={[0, spacing * 3, 0]} />
          <LowerFloor position={[0, spacing * 2, 0]} />
          <LowerFloor position={[0, spacing, 0]} />
          <LowerFloor position={[0, 0, 0]} />
        </group>
        <group ref={refs.restOfTower} position={defaultPositions.restOfTower}>
          {restOfTower}
        </group>
      </group>
    </>
  );
};

export default Tower;
