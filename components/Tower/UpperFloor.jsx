import React, { forwardRef } from "react";
import { useGLTF } from "@react-three/drei";
import Outline from "./Outline";
import useBakedTexture from "./useBakedTexture";

const UpperFloor = forwardRef(
  ({ showBalcony = false, showCovers = false, ...props }, ref) => {
    const { nodes } = useGLTF("/models/upperfloor_model.glb");
    const material = useBakedTexture("/models/upperfloor_bake.jpg");
    return (
      <group ref={ref} {...props} dispose={null}>
        <Outline
          geometry={nodes.Floor.geometry}
          rotation={[Math.PI / 2, 0, 0]}
        />

        <mesh
          geometry={nodes.Floor.geometry}
          material={material}
          rotation={[Math.PI / 2, 0, 0]}
        />
        <mesh
          scale={1.0}
          geometry={nodes.Windows.geometry}
          material={material}
          rotation={[Math.PI / 2, 0, 0]}
        />

        {showBalcony ? (
          <>
            <mesh
              geometry={nodes.Balcony.geometry}
              material={material}
              rotation={[Math.PI / 2, 0, 0]}
            />
            <Outline
              geometry={nodes.Balcony.geometry}
              rotation={[Math.PI / 2, 0, 0]}
            />
          </>
        ) : null}
        {showCovers ? (
          <>
            <mesh
              geometry={nodes.Covers.geometry}
              material={material}
              rotation={[Math.PI / 2, 0, 0]}
            />
            <Outline
              geometry={nodes.Covers.geometry}
              rotation={[Math.PI / 2, 0, 0]}
            />
          </>
        ) : null}
      </group>
    );
  }
);

UpperFloor.displayName = "UpperFloor";

useGLTF.preload("/models/upperfloor_model.glb");

export default UpperFloor;
