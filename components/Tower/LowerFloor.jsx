import React, { forwardRef } from "react";
import { useGLTF } from "@react-three/drei";
import useBakedTexture from "./useBakedTexture";
import Outline from "./Outline";

const LowerFloor = forwardRef((props, ref) => {
  const { nodes } = useGLTF("/models/lowerfloor_model.glb");

  const material = useBakedTexture("/models/lowerfloor_bake.jpg");

  return (
    <group ref={ref} {...props} dispose={null}>
      <Outline
        geometry={nodes.Floor001.geometry}
        rotation={[Math.PI / 2, 0, 0]}
      />

      <mesh
        geometry={nodes.Floor001.geometry}
        material={material}
        rotation={[Math.PI / 2, 0, 0]}
      />
      <mesh
        geometry={nodes.Windows001.geometry}
        material={material}
        rotation={[Math.PI / 2, 0, 0]}
      />
    </group>
  );
});

LowerFloor.displayName = "LowerFloor";

useGLTF.preload("/models/lowerfloor_model.glb");
export default LowerFloor;
