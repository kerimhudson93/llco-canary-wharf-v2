import { useEffect, useState } from "react";
import { useCurrentPage, usePage, usePageUpdater } from "../atoms";

const links = [
  {
    label: "Video",
    id: "video",
  },
  {
    label: "Overview",
    id: "overview",
  },
  {
    label: "Floor 47",
    id: "floor47",
  },
  {
    label: "Floor 48",
    id: "floor48",
  },
  {
    label: "Floor 49",
    id: "floor49",
  },
  {
    label: "Floor 45-46",
    id: "floor45",
  },
  {
    label: "Floor 40-43",
    id: "floor40",
  },
  {
    label: "Contact",
    id: "contact",
  },
];

const SideNavigation = () => {
  const { page, pages } = usePage();
  const [hide, setHide] = useState(true);
  const hideSideNavigation =
    page.number === 0 || page.number === 1 || page.number > 14 || hide;

  useEffect(() => {
    setTimeout(() => {
      setHide(false);
    }, 3000);
  }, []);

  return (
    <div
      className={[
        "hidden lg:flex fixed top-1/2 left-0  flex-col gap-4 pl-8 transform -translate-y-1/2 group z-50 transition-opacity duration-300",
        hideSideNavigation ? "opacity-0" : "opacity-100",
      ].join(" ")}
    >
      {links.map((link) => {
        const currentPage = pages[page.number] === link.id;
        return (
          <a
            href={`#${link.id}`}
            key={link.id}
            className={[
              currentPage
                ? "border-l-4 border-darkgreen"
                : "border-l-2 border-orange hover:border-darkgreen",
              "py-1",
            ].join(" ")}
          >
            <div
              className={[
                currentPage
                  ? "opacity-100"
                  : "opacity-0 group-hover:opacity-100 ",
                "transition-opacity duration-200",
              ].join(" ")}
            >
              <span
                className={[
                  currentPage ? "opacity-100" : "opacity-70 hover:opacity-100 ",
                  "px-4 uppercase",
                ].join(" ")}
              >
                {link.label}
              </span>
            </div>
          </a>
        );
      })}
    </div>
  );
};

export default SideNavigation;
