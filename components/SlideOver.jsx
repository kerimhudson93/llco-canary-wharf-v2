import { Fragment, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { usePoints } from "../atoms";

const SlideOver = () => {
  const { showActivePointOverlay, setShowActivePointOverlay } = usePoints();
  return (
    <Transition.Root show={showActivePointOverlay} as={Fragment}>
      <Dialog
        as="div"
        className="fixed inset-0 overflow-hidden z-50"
        onClose={setShowActivePointOverlay}
      >
        <div className="absolute inset-0 overflow-hidden">
          <div className="fixed inset-y-0 right-0 max-w-full flex">
            <Transition.Child
              as={Fragment}
              enter="transform transition ease-in-out duration-500 sm:duration-700"
              enterFrom="translate-x-full"
              enterTo="translate-x-0"
              leave="transform transition ease-in-out duration-500 sm:duration-700"
              leaveFrom="translate-x-0"
              leaveTo="translate-x-full"
            >
              <div className="w-screen">
                <div className="h-full flex flex-col bg-white shadow-xl overflow-y-scroll">
                  <div
                    className="w-8 bg-darkgreen h-screen flex items-center justify-center"
                    onClick={() => setShowActivePointOverlay(false)}
                  >
                    <span className="transform -rotate-90 w-full text-white font-medium whitespace-nowrap pb-1">
                      Close Panel
                    </span>
                  </div>
                </div>
              </div>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
};

export default SlideOver;
