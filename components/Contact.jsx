const Contact = () => {
  return (
    <div
      id="contact"
      className="relative h-screen w-screen bg-darkgreen z-50 flex items-center justify-center px-8"
    >
      <div className="flex flex-col items-center justify-center">
        <h3 className="text-3xl font-normal xl:text-8xl text-cream uppercase xl:font-light mb-12">
          Meet us at the top
        </h3>
        <p className="text-lg text-cream max-w-lg w-full text-center font-light mb-16">
          For more information or to book an appointment with our sales team,
          please fill in the form below and our team will be in touch.
        </p>
        <form className="flex flex-col items-center gap-8 w-full max-w-2xl mb-32">
          <input
            className="w-full placeholder-cream bg-darkgreen border-b border-cream text-center text-base lg:text-lg xl:text-xl font-light py-2 uppercase"
            type="text"
            placeholder="Your full name"
          />
          <input
            className="w-full placeholder-cream bg-darkgreen border-b border-cream text-center text-base lg:text-lg xl:text-xl font-light py-2 uppercase"
            type="text"
            placeholder="Your email address"
          />
          <input
            className="w-full placeholder-cream bg-darkgreen border-b border-cream text-center text-base lg:text-lg xl:text-xl font-light py-2 uppercase"
            type="text"
            placeholder="Phone number"
          />
          <button
            type="submit"
            className="bg-cream text-darkgreen py-2 px-32 uppercase tracking-wider text-base lg:text-lg xl:text-xl mt-8"
          >
            Submit
          </button>
        </form>
        <small className="text-cream text-base text-center font-light">
          Privacy is very important to us, we will never share your details with
          any third parties.
        </small>
      </div>
    </div>
  );
};

export default Contact;
