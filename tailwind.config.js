module.exports = {
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      sans: ["Founders Grotesk", "Inter", "sans-serif"],
    },
    extend: {
      zIndex: {
        100: "100",
      },
      height: {
        "2screen": "200vh",
      },
      colors: {
        cream: "#FDF7F1",
        darkgreen: "#303B3C",
        orange: "#FAC181",
      },
      keyframes: {
        fadeout: {
          "0%": { opacity: 1 },
          "100%": { opacity: 0 },
        },
        fadein: {
          "0%": { opacity: 0 },
          "100%": { opacity: 1 },
        },
      },
      animation: {
        fadeout: "fadeout 3s ease-in-out forwards 5s",
        fadein: "fadein 0.3s ease-in-out forwards 0.2s",
      },
      width: {
        video: "177vw",
      },
      transitionDelay: {
        3000: "3000ms",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
