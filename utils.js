import * as THREE from "three";

export const lerp = (ref, value) => THREE.MathUtils.lerp(ref, value, 0.2);
